from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/")
def hello_world():
    app.logger.warn('Headers: %s', request.headers)
    return "<p>TimeWeb Cloud Apps ❤️ Flask!</p>"